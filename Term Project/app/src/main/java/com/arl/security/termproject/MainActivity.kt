package com.arl.security.termproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {


    private var student_btn: Button? = null
    private var admin_btn: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        student_btn = findViewById(R.id.studnet_btn)
        admin_btn = findViewById(R.id.admin_btn)


        student_btn?.setOnClickListener {
            intent = Intent(applicationContext, StudentPage::class.java)
            startActivity(intent)
        }

        admin_btn?.setOnClickListener {
            intent = Intent(applicationContext, AdminCreate::class.java)
            startActivity(intent)
        }


    }
}
