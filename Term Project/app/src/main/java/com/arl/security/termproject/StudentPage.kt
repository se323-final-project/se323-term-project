package com.arl.security.termproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationView

class StudentPage : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_page)

        val nav = findViewById<BottomNavigationView>(R.id.nav)
        val frame = findViewById<FrameLayout>(R.id.frame_layout)
        val all_activity = all_activity()
        val my_list =  my_list()
        val score = score()

        val fragmentChanger = supportFragmentManager.beginTransaction()
        fragmentChanger.replace(R.id.frame_layout, all_activity)
        fragmentChanger.commit()

        nav.setOnNavigationItemSelectedListener { item ->

            when (item.itemId) {

                R.id.all_activity_icon -> {

                    val fragmentChanger = supportFragmentManager.beginTransaction()
                    fragmentChanger.replace(R.id.frame_layout, all_activity)
                    fragmentChanger.commit()

                    true
                }

                R.id.my_list_icon -> {

                    val fragmentChanger = supportFragmentManager.beginTransaction()
                    fragmentChanger.replace(R.id.frame_layout, my_list)
                    fragmentChanger.commit()

                    true
                }

                R.id.score_icon -> {

                    val fragmentChanger = supportFragmentManager.beginTransaction()
                    fragmentChanger.replace(R.id.frame_layout, score)
                    fragmentChanger.commit()

                    true
                }

                else -> false

            }
        }

    }
}

