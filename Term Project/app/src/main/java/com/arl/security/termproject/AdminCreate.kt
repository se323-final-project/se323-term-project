package com.arl.security.termproject

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class AdminCreate : AppCompatActivity() {

    private var name: TextView? = null
    private var id: TextView? = null
    private var semester: TextView? = null
    private var maximum: TextView? = null
    private var credit: TextView? = null
    private var create_btn: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_create)

        name = findViewById(R.id.name)
        id = findViewById(R.id.id)
        semester = findViewById(R.id.semester)
        maximum = findViewById(R.id.maximum)
        credit = findViewById(R.id.credit)
        create_btn = findViewById(R.id.create)

        var name_text = name
//        var id_text = id?
//        var semester_text = semester?.text
//        var maximum_text = maximum?.text
//        var credit_text = credit?.text

        create_btn?.setOnClickListener {
            Log.d("testttttt","${name_text},${id},${semester},${maximum},${credit}")
            CreateActivity(name, id, semester,  maximum, credit)
        }

    }

    private fun CreateActivity(
        aName: TextView?,
        aID: TextView?,
        aSemester: TextView?,
        aMaximum: TextView?,
        aCredit: TextView?
    ) {
        if (TextUtils.isEmpty(aName.toString()) || TextUtils.isEmpty(aID.toString()) || TextUtils.isEmpty(
                aSemester.toString()
            ) || TextUtils.isEmpty(
                aMaximum.toString()
            ) || TextUtils.isEmpty(aMaximum.toString()) || TextUtils.isEmpty(aCredit.toString())
        ) {
            Log.d("Detail-information","${aName},${aID},${aSemester},${aMaximum},${aCredit}")

            Alert("Create activity fail", "Please fill all input")

        } else {
            Toast.makeText(applicationContext, "Create successful", Toast.LENGTH_SHORT).show();
//            finish();
//            startActivity(intent);
        }
    }

    private fun Alert(title: String, message: String) {

        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage(message)
            .setCancelable(false)
            .setNegativeButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        val alert = dialogBuilder.create()
        alert.setTitle(title)
        alert.show()

    }
}
