package com.arl.security.termproject

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * A simple [Fragment] subclass.
 */
class all_activity : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val layout = inflater.inflate(R.layout.fragment_all_activity, container, false)



        var items = ArrayList<Activity_Class>()

        items.add(Activity_Class("Freshman activity", "001", "2563/1", "200", "1/hr", "Enroll"))
        items.add(Activity_Class("Marathon", "002", "2563/1", "100", "5/hr", "Enroll"))
        items.add(Activity_Class("Songkran activity", "003", "2562/2", "200", "3/hr", "Enroll"))
        items.add(Activity_Class("Watching movie", "004", "2563/1", "100", "1/hr", "Enroll"))

        val recycler = layout.findViewById<RecyclerView>(R.id.activity_list)


        val adapter = ActivityAdapter(items, activity!!.applicationContext)

        recycler.layoutManager = GridLayoutManager(activity!!.applicationContext, 1)

        recycler.adapter = adapter

        return layout
    }

}
