package com.arl.security.termproject

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView

//class ActivityAdapter (context: Context, data:ArrayList<Activity_Class>) : ArrayAdapter<Activity_Class>(context,0,data){
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//
//        var itemView = convertView
//
//        itemView = LayoutInflater.from(context).inflate(R.layout.activity_item,parent,false)
//
//        val item = getItem(position)
//
//        val title = itemView!!.findViewById<TextView>(R.id.activity_name)
//        title.text = item!!.name
//
//        val id = itemView!!.findViewById<TextView>(R.id.activity_id)
//        id.text = item!!.id
//
//        val semester = itemView!!.findViewById<TextView>(R.id.activity_semester)
//        semester.text = item!!.semester
//
//        val maximum = itemView!!.findViewById<TextView>(R.id.maximum)
//        maximum.text = item!!.maximum
//
//        val credit = itemView!!.findViewById<TextView>(R.id.credit)
//        credit.text = item!!.credit
//
////        val image = itemView!!.findViewById<ImageView>(R.id.user_image)
////        image.setImageResource(item.user_image)
//
//        return itemView
//
//    }
//}

class ActivityAdapter (data:ArrayList<Activity_Class>, var context: Context) : RecyclerView.Adapter<ActivityAdapter.ViewHolder>() {

    var data:List<Activity_Class>

    init {
        this.data = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layout = LayoutInflater.from(context).inflate(R.layout.activity_item,parent,false)

        return ViewHolder(layout)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.name.text = data[position].name
        holder.id.text = data[position].id
        holder.semester.text = data[position].semester
        holder.maximum.text = data[position].maximum
        holder.credit.text = data[position].credit
        holder.button_text.text = data[position].btn_text

        holder.button_text?.setOnClickListener {
            Toast.makeText(context, "Remove successful", Toast.LENGTH_SHORT).show();
        }


//        holder.image.setImageResource(data[position].image)

    }

    override fun getItemCount(): Int {

        return data.size

    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        var name: TextView
        var id:TextView
        var semester:TextView
        var maximum:TextView
        var credit:TextView
        var button_text: Button

        init {

            name = itemView.findViewById(R.id.activity_name)
            id = itemView.findViewById(R.id.activity_id)
            semester = itemView.findViewById(R.id.activity_semester)
            maximum = itemView.findViewById(R.id.maximum)
            credit = itemView.findViewById(R.id.credit)
            button_text = itemView.findViewById(R.id.text_btn)

        }

    }
}